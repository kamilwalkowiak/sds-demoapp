<?php

namespace App\Api;

/**
 * Class Client
 * @package App\Api
 */
class Client
{
    private $client;
    private $response;
    private $body;

    /**
     * Client constructor.
     * @param $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * @param $path
     * @param array $options
     * @return $this
     */
    public function query($path, $options = [])
    {
        $endpoint = $path . '?' . http_build_query($options);

        $this->response = $this->client->get($endpoint);

        return $this;
    }

    /**
     * @return array|bool|null
     */
    public function getBody()
    {
        if (null === $this->body) {
            $this->body = json_decode($this->response->getBody(), true);
        }

        return $this->body;
    }

    /**
     * @return array|bool|null
     */
    public function getData()
    {
        $body = $this->getBody();

        return $this->isList($body) ? $body['data'] : $body;
    }

    /**
     * @return integer
     */
    public function getCurrentPage()
    {
        return $this->getBody()['current_page'];
    }

    public function getPages()
    {
        $body = $this->getBody();

        return ceil($body['total'] / $body['per_page']);
    }

    /**
     * @param array $array
     * @return bool
     */
    private function isList($array)
    {
        return (!array_diff(['first_page_url', 'last_page_url', 'next_page_url', 'data'], array_keys($array)));
    }
}