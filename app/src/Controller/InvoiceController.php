<?php

namespace App\Controller;

use App\Entity\invoice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Api;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class invoiceController
 * @package App\Controller
 *
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/", name="invoice_list")
     */
    public function index(Request $request)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $query = $client->query(
            'invoices',
            [
                'page' => $request->get('page'),
                'per_page' => $request->get('items')
            ]
        );

        return $this->render('Invoice/index.html.twig', [
            'invoices' => $query->getData()
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/{id}", name="invoice_show", requirements={"id" = "\d+"})
     */
    public function show(Request $request)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $query = $client->query('invoices/' . $request->get('id'));

        return $this->render('Invoice/show.html.twig', [
            'invoice' => $query->getData()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     * @return Response
     *
     * @Route("/{id}/import", name="invoice_import", requirements={"id" = "\d+"})
     */
    public function import(Request $request, EntityManagerInterface $em, FlashBagInterface $flashBag, TranslatorInterface $translator)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $invoice = $client
            ->query('invoices/' . $request->get('id'))
            ->getData();

        $newInvoice = new Invoice();
        $newInvoice->setNumber($invoice['number']);

        //@todo: we should import Customer before we create relation

        $em->persist($newInvoice);
        $em->flush();

        $flashBag->add('info', $translator->trans('invoice.import.info'));

        return $this->redirectToRoute('invoice_show', ['id' => $invoice['id']]);
    }
}
