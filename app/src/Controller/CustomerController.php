<?php

namespace App\Controller;

use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Api;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CustomerController
 * @package App\Controller
 *
 * @Route("/customer")
 */
class CustomerController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/", name="customer_list")
     */
    public function index(Request $request)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $query = $client->query(
            'customers',
            [
                'page' => $request->get('page'),
                'per_page' => $request->get('items')
            ]
        );

        return $this->render('Customer/index.html.twig', [
            'customers' => $query->getData(),
            'current_page' => $query->getCurrentPage(),
            'pages' => $query->getPages()
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/{id}", name="customer_show", requirements={"id" = "\d+"})
     */
    public function show(Request $request)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $query = $client->query('customers/' . $request->get('id'));

        return $this->render('Customer/show.html.twig', [
            'customer' => $query->getData()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     * @return Response
     *
     * @Route("/{id}/import", name="customer_import", requirements={"id" = "\d+"})
     */
    public function import(Request $request, EntityManagerInterface $em, FlashBagInterface $flashBag, TranslatorInterface $translator)
    {
        $client = new Api\Client($this->get('eight_points_guzzle.client.test_api'));

        $customer = $client
            ->query('customers/' . $request->get('id'))
            ->getData();

        $newCustomer = new Customer();
        $newCustomer->setCompanyName($customer['company_name']);
        $newCustomer->setCustomerNo($customer['customer_no']);
        $newCustomer->setForeignId($customer['id']);

        $em->persist($newCustomer);
        $em->flush();

        $flashBag->add('info', $translator->trans('customer.import.info'));

        return $this->redirectToRoute('customer_show', ['id' => $customer['id']]);
    }
}
