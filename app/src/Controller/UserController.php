<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Form\UserType;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/profile")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_show")
     */
    public function show()
    {
        $user = $this->getUser();

        return $this->render('User/profile.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * @param Request $request
     * @param FlashBagInterface $flashBag
     * @param TranslatorInterface $translator
     * @return Response
     *
     * @Route("/edit", name="user_edit")
     */
    public function update(Request $request, FlashBagInterface $flashBag, TranslatorInterface $translator)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $flashBag->add('info', $translator->trans('user.updated'));

            return $this->redirectToRoute('user_show');
        }

        return $this->render('User/update.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}