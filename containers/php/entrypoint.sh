#!/bin/sh

# install dependencies, migrate database schema and load fixtures
composer install --no-interaction --optimize-autoloader --prefer-dist \
    && npm install \
    && php bin/console doctrine:migrations:migrate --no-interaction \
    && php bin/console doctrine:fixtures:load --no-interaction \
    && npm run watch

php-fpm