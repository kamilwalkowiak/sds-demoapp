# README #

SDS demo app.

Here you can find quick summary of task, deployment instructions and current progress.

This task took so far 18h, however not all requirements are in place. 

This is my first project where I took Symfony 4 as framework. Previously I worked mostly with 2.8.
Also first time I decided to not install FOSUserBundle for user management. I build user module on top of Security component directly.


### What was done ###

* i18n Support - English and Polish
* Login, profile, editing user properties
* Customer list
* Customer detail
* Invoices - partially
* UI
* Preparation for ajax search

### What is missing ###

* Login via Ajax
* Merge of entities from API and local DB
* Invoices - due to API throwing 500 :(
* Ajax search

### What should be done better ###

Due to short time I didn't build all things I had in mind:

* All forms require CSRF validation (currently turned off)
* Return all data via REST and build React based UI and get rid of Templating component
* Implement REST Client as middleware for entities to fetch data by Doctrine that allows us to describe Entities with Annotations how to fetch data from API and merge before it touch Controller

### Deployment requirements ###

* Docker

### How to run project - dockerized version ###

* Add entry to /etc/hosts (or equivalent): `sds-demoapp.local 127.0.0.1`
* Pull project to any directory with your projects
* Go to main directory of sds app
* Run `docker-compose up --build` to build and run project in docker container
* Wait till all images are being build :)
* SET APitoken in .env file
* Go to http://sds-demoapp.local

### Demo user ###

During setup of project, demo user will be created with credentials:
* login: demo@examle.com
* pass: demo123